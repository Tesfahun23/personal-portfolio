"use client";

import { useState } from "react";
import Footer from "./components/footer";
import Navbar from "./components/navbar";
import Image from "next/image";
import AboutSection from "./components/about-section";

export default function Home() {


  return (
    <main className="px-1">
      <Navbar />
      <div className="p-6 my-3 mx-auto xl:w-2/3 lg:w-2/3 md:w-full sm:w-full xs:w-full 2xl:w-1/2">
        <div className="flex lg:flex-row md:flex-col sm:flex-col xs:flex-col gap-10 my-3 lg:justify-center sm:items-center xs:items-center">
          <div className="pa-1 border-4 border-white-500 rounded-full drop-shadow-[10px_10px_10px_rgba(255,255,255,0.2)] ">
            <Image
              className="bg-slate-500 rounded-full transition duration-300 grayscale hover:grayscale-0"
              src="/avatar.webp"
              alt="Avatar of tesfahun"
              width={200}
              height={200}
            />
          </div>

          <div>
            <div
              className="flex lg:flex-col sm:flex-row xs:flex-row lg:gap-0 xl:gap-0 2xl:gap-0 gap-2"
              id="full-name"
            >
              <div className="text-white text-sm font-normal lg:inline sm:hidden xs:hidden ">
                <span className="text-lg mr-1">👋🏽</span> Hiya , Its-A Me,
              </div>
              <div>Tesfahun</div>

              <div>Kebede</div>
            </div>
          </div>
        </div>
        <div className="text-sm font-thin text-center my-2 ">
          A freelance software developer with four years of experience in
          designing, building, and integrating apps using Flutter, VueJs and
          React.
        </div>
        <div className="flex justify-center">
          <div className="border-b-[0.5px] border-white-500 w-3/4"></div>
        </div>

        <AboutSection />
      </div>

      <Footer />
    </main>
  );
}
