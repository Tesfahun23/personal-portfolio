import React from "react";

import { all_projects } from "../../data/projects";

import { HiSwatch } from "react-icons/hi2";
import ProjectCard from "./project-card";

export default function Projects() {
  return (
    <div id="projects" className="my-6">
      <h1 className="text-2xl text-bold my-2 flex ">
        <span className="mx-2">
          <HiSwatch className="text-white h-7 w-7" />
        </span>
        Projects
      </h1>

      {all_projects.map((value, index) => (
        <ProjectCard key={index} project={value} />
      ))}
    </div>
  );
}
