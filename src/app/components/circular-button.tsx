import React from "react";

interface Props {
  child: React.ReactNode;
  onClick?: () => void;
  length?: number;
  borderWidth?: string;
}

function CircleButton({
  child,
  onClick,
  length = 9,
  borderWidth = "1.2px",
}: Props) {
  return (
    <button
      onClick={onClick}
      className={`border-[${borderWidth}] p-2 border-slate-900 h-${length} w-${length} rounded-full flex justify-center items-center`}
    >
      {child}
    </button>
  );
}

export default CircleButton;
