import { BsFillBuildingsFill } from "react-icons/bs";
import React from "react";
import { HiWrench } from "react-icons/hi2";
import { IProject } from "../../../types/project";
import { FaLaptopCode } from "react-icons/fa";

interface IProjectCardProps {
  project: IProject;
}

export default function ProjectCard({ project }: IProjectCardProps) {
  return (
    <div
      id={project.slug}
      className="border rounded-lg border-slate-500 px-2 py-4  my-6"
    >
      <div className="flex flex-wrap gap-2 items-center">
        <FaLaptopCode
          size={30}
          className=" transition duration-300 ease-in-out text-white border-2 border-white-500 rounded-full p-1"
        />
        <div className="flex flex-col">
          <div className="text-md font-bold text-slate-300 mb-1">
            {project.name}
          </div>

          <div className="text-xs text-slate-500 font-semi-bold">
            {project.duration}
          </div>
        </div>
      </div>

      <div className="text-sm my-1">{project.description}</div>

      <div className="ml-1 flex flex-row items-center my-2">
        <HiWrench className="text-white" />
        <div className="text-white ml-2 text-sm font-semibold ">Tools Used</div>
      </div>

      <div className="flex flex-wrap gap-2">
        {project.tools.map((value, index) => (
          <div
            key={index}
            className="px-3 py-1 border border-white-200 rounded-full flex flex-wrap gap-1 items-center"
          >
            {value.icon}
            <div className="text-white text-xs"> {value.name}</div>
          </div>
        ))}
      </div>

      {/* <div className="ml-1 flex flex-row items-center my-2">
        <HiSwatch className="text-white" />
        <div className="text-white ml-2 text-sm font-semibold ">
          Tools & Technologies
        </div>
      </div> */}
    </div>
  );
}
