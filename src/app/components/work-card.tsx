import { BsFillBuildingsFill } from "react-icons/bs";
import React from "react";
import { HiSwatch } from "react-icons/hi2";
import { Link } from "react-scroll";
import { IWorkExperience } from "../../../types/work";

interface IWorkCardProps {
  xp: IWorkExperience;
}

export default function WorkCard({ xp }: IWorkCardProps) {
  return (
    <div  className="border rounded-lg border-slate-500 px-2 py-4  my-6">
      <div className="flex flex-wrap gap-2 items-center">
        <BsFillBuildingsFill
          size={40}
          className=" transition duration-300 ease-in-out text-white border-2 border-white-500 rounded-full p-1"
        />
        <div className="flex flex-col">
          <div className="flex xl:flex-row lg:flex-row md:flex-row sm:flex-col xs:flex-col">
            <div className="text-xs mr-1 text-slate-400 font-semibold">
              {xp.company}
            </div>
            <div className="text-xs text-slate-500 font-semi-bold">
              ( {xp.duration})
            </div>
          </div>

          <div className="text-md font-bold text-slate-300 mb-1">
            {xp.position}
          </div>
        </div>
      </div>

      <ul className="my-2">
        {xp.responsiblities.map((value, index) => (
          <li key={index} className="text-xs text-gray-300 mb-2">
            {value}
          </li>
        ))}
      </ul>

      <div className="ml-1 flex flex-row items-center mb-2">
        <HiSwatch className="text-white" />
        <div className="text-white ml-2 text-sm font-semibold ">Projects</div>
      </div>

      <div className="flex flex-wrap gap-2">
        {xp.projects.map((value, index) => (
          <Link
            key={index}
            to={value.slug}
            className="px-3 border bg-none  text-white border-white-200 rounded-full transition ease-in-out hover:bg-white hover:text-black duration-300"
            spy={true}
            smooth={true}
            duration={600}
          >
            <span className="text-xs"> {value.name}</span>
          </Link>
        ))}
      </div>

      {/* <div className="ml-1 flex flex-row items-center my-2">
        <HiSwatch className="text-white" />
        <div className="text-white ml-2 text-sm font-semibold ">
          Tools & Technologies
        </div>
      </div> */}
    </div>
  );
}
