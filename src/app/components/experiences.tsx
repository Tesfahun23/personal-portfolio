import React from "react";
import WorkCard from "./work-card";
import jobs from "../../data/work-experiences";

import { HiBriefcase } from "react-icons/hi2";
function WorkExperiences() {
  return (
    <div id="work" className="my-6">
      <h1 className="text-2xl text-bold my-2 flex ">
        <span className="mx-2">
          <HiBriefcase className="text-white h-7 w-7" />
        </span>
        Work
      </h1>

      {jobs.map((value, index) => (
        <WorkCard key={index} xp={value} />
      ))}
    </div>
  );
}

export default WorkExperiences;
