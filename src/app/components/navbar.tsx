import { useState } from "react";

import { Link } from "react-scroll";

import {
  HiBriefcase,
  HiCommandLine,
  HiSwatch,
  HiSun,
  HiMoon,
} from "react-icons/hi2";
import CircleButton from "./circular-button";

export default function Navbar() {
  const [darkMode, setDarkMode] = useState(true);

  function toggleDarkMode() {
    setDarkMode((prevDarkMode) => !prevDarkMode);
  }

  return (
    <nav className="mt-3 bg-white w-full rounded-lg ">
      <div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
        <div className="relative flex items-center justify-between h-16">
          <div className="flex-shrink-0">
            <a href="/" className="flex items-center">
              <HiCommandLine className="h-10 w-10 text-slate-900" />
              <span className="text-black ml-3 font-semibold">Home</span>
            </a>
          </div>
          <div className="flex items-center">
            <div className="flex-shrink-0">
              <Link
                to="work"
                className="text-black hover:text-white px-3 py-2 rounded-md text-sm font-medium flex items-center "
                spy={true}
                smooth={true}
                duration={500}
              >
                <HiBriefcase className="h-3 w-3 text-slate-900" />
                <div className="text-black ml-3 font-semibold">Work</div>
              </Link>
            </div>
            <div className="flex-shrink-0">
              <Link
                to="projects"
                className="text-black hover:text-white px-3 py-2 rounded-md text-sm font-medium flex items-center "
                spy={true}
                smooth={true}
                duration={500}
              >
                <HiSwatch className="h-3 w-3 text-slate-900" />
                <div className="text-black ml-3 font-semibold">Projects</div>
              </Link>
            </div>
            <div className="flex-shrink-0 ml-2">
              <CircleButton
                onClick={toggleDarkMode}
                child={
                  darkMode ? (
                    <HiSun className="h-4 w-4 text-slate-900" />
                  ) : (
                    <HiMoon className="h-4 w-4 text-slate-900" />
                  )
                }
              />
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}
