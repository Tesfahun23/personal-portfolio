import React from "react";



import Projects from "./projects";
import { FaBookOpen, FaCalendarDay, FaGraduationCap, FaUser } from "react-icons/fa";
import { HiBuildingLibrary, HiSwatch, HiWrenchScrewdriver } from "react-icons/hi2";
import WorkExperiences from "./experiences";

export default function AboutSection() {
  const about =
    "I am a self-taught developer who is passionate about picking up new knowledge and techniques. I have developed a solid understanding of software development principles throughout the course of my career and I am constantly looking for ways to broaden my knowledge and skill set. I'm dedicated to providing top-notch software solutions and have a solid foundation in front end development as well as a desire to stay current with market trends. Where I can use my creativity and problem-solving abilities to take on challenging tasks, in dynamic and collaborative contexts, I flourish. I am keen to give my expertise and continue developing as a developer to promote innovation and success in the area since I am very curious and want to have a positive influence.";
  return (
    <div>
      <div className="my-6">
        <h1 className="text-2xl text-bold my-2 flex ">
          <span className="mx-2">
            <FaUser className="text-white h-7 w-7" />
          </span>
          About me
        </h1>

        <div className="text-sm text-light">{about}</div>
      </div>
      <div className="my-6">
        <h1 className="text-2xl text-bold my-2 flex ">
          <span className="mx-2">
            <FaGraduationCap className="text-white h-7 w-7" />
          </span>
          Education
        </h1>
        <div className="text-sm text-light">
          <div className="flex flex-wrap items-center gap-2 ">
            <div>
              <HiBuildingLibrary className="text-white-400 h-4 w-4" />
            </div>

            <div>Addis Ababa Science and Technology University</div>
          </div>
          <div className="flex flex-wrap items-center gap-2 my-2 ">
            <div>
              <FaBookOpen className="text-white-400 h-4 w-4" />
            </div>

            <div>BSC in Computer Science & IT</div>
          </div>
          <div className="flex flex-wrap items-center gap-2 my-2 ">
            <div>
              <FaCalendarDay className="text-white-400 h-4 w-4" />
            </div>

            <div>June 2019 GC</div>
          </div>
        </div>
      </div>

      <WorkExperiences/>

      <Projects />

      <div className="my-6">
        <h1 className="text-2xl text-bold my-2 flex ">
          <span className="mx-2">
            <HiSwatch className="text-white h-7 w-7" />
          </span>
          Other Projects( Honorable Mentions )
        </h1>
        Side Projects Go Here
        {/* Content Goes Here  */}
      </div>
      <div className="my-6" id="about">
        <h1 className="text-2xl text-bold my-2 flex ">
          <span className="mx-2">
            <HiWrenchScrewdriver className="text-white h-7 w-7" />
          </span>
          Tools & Technologies
        </h1>
        Tools I have used go here
        {/* Content Goes Here  */}
      </div>
    </div>
  );
}
