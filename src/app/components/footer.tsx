import React from "react";

import { FaLinkedinIn, FaGithub, FaGitlab, FaTelegram } from "react-icons/fa";

import { MdEmail } from "react-icons/md";
import { BsTelephoneFill } from "react-icons/bs";
function Footer() {
  return (
    <footer className="mx-auto py-3 bg-white my-3 left-0 right-0 bottom-0 xl:w-2/3 lg:w-2/3 md:w-full sm:w-full xs:w-full 2xl:w-1/2 rounded-b-lg">
      <div className="flex justify-center items-center gap-2">
        <a href="mailto:tesfahunkebede336@gmail.com" className="text-black px-1">
          <MdEmail color="#000" size={25} />
        </a>
        <a href="tel:+251910117158" className="text-black px-1">
          <BsTelephoneFill color="#000" size={22} />
        </a>
        <a href="https://t.me/xenoceph" className="text-black px-1">
          <FaTelegram color="#000" size={25} />
        </a>
        <a href="https://www.linkedin.com/in/tesfahun-kebede/" className="text-black px-1">
          <FaLinkedinIn  color="#000" size={25} />
        </a>
        <a href="https://github.com/TesfahunK" className="text-black px-1">
          <FaGithub color="#000" size={25} />
        </a>
        <a href="https://gitlab.com/Tesfahun23" className="text-black px-1">
          <FaGitlab color="#000" size={25} />
        </a>
      </div>
    </footer>
  );
}

export default Footer;
