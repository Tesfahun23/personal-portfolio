import { IProject } from "../../types/project";
import tools from "./tools";

export let minab_projects: IProject[] = [
  {
    slug: "nrp",
    name: "NRP (then Overseas)",
    description:
      "JCC NRP is a platform for both local and overseas jobs seekers to register and take automated assessments for digital workforce mapping, screening as well as job placement. It is designed to register, build profiles, provide free screening, keep track of all training, testing & certification of overseas job seekers while digitalizing the documentation process for legal and safe ways of working abroad. In doing so the platform will also be able to map out the national jobseeker landscape and provide a data-driven job matching public services for both local and foreign needs. Job seekers were able to register, build their profile, take automated assessments and get matched to a job.",
    duration: "Feb 2020 - Oct 2020",
    tools: [
      tools.dart,
      tools.flutter,
      tools.vuetify,
      tools.hasura,
      tools.graphql,
      tools.express,
      tools.loopback,
      tools.gitlab,
    ],
  },
  {
    slug: "m4cd",
    name: "Hahu Jobs University (M4CD)",
    description:
      "HahuJobs University is a platform that is intended to provide a much-needed remote digital content provision for students both in university and TVET schools in the wake of this pandemic which largely affected their learning process as all students were made to stay at home.Universities will register and get verified, and will post course contents (video, pdf, images etc), announcements etc to their students or even to the public. Students can download and comment on posts and announcements.",
    duration: "May 2020 - Dec 2020",
    tools: [
      tools.dart,
      tools.flutter,
      tools.vuetify,
      tools.hasura,
      tools.graphql,
      tools.express,
      tools.gitlab,
    ],
  },
  {
    slug: "sme-cip",
    name: "SME-CIP",
    description:
      "SME-CIP is mobile portal for registering Small and Mid-sized Enterprises, in this platform SMEs register their products and services thus making their information available suppliers and service providers and vice versa. I was responsible for designing and implementing the mobile app that handled registration and showing SME information.",
    duration: "Aug 2020 - Nov 2020",
    tools: [
      tools.dart,
      tools.flutter,
      tools.playstore,
      tools.hasura,
      tools.graphql,
      tools.express,
      tools.gitlab,
    ],
  },
  {
    slug: "job-protection-facility",
    name: "Jobs Protection Facility",
    description:
      "Jobs Protection Facility has been established in response to the threat posed by the COVID-19 pandemic to Ethiopia's manufacturing sector and in turn to the security of jobs for factory workers. The objectives of the Facility are twofold, To sustain livelihoods and productivity of factory workers through the COVID-19 crisis, protecting jobs for at least six months. and To ensure that manufacturing capacity in Ethiopia is preserved, and positioned well to contribute to the recovery of the economy post-COVID-19.",
    duration: "Aug 2020 - Nov 2020",
    tools: [
      tools.hasura,
      tools.vuetify,
      tools.nuxtjs,
      tools.vuejs,
      tools.graphql,
      tools.gitlab,
      tools.netlify,
    ],
  },
];

export let gcme_projects: IProject[] = [
  {
    slug: "deep-life",
    name: "DeepLife",
    description:
      "DeepLife is a gospel discipleship platform that lets ministries and minsters to register new disciples based on their faith status. ",
    duration: "Aug 2020 - Nov 2020",
    tools: [
      tools.flutter,
      tools.dart,
      tools.sqlite,
      tools.express,
      tools.bitbucket,
    ],
  },
];
export let code_light_projects: IProject[] = [
  {
    slug: "codelight-evd",
    name: "DeepLife",
    duration: "Dec 2020 - Present",
    description:
      "This is project that focuses on mobile airtime voucher distribution , i was responsible for developing 3 different apps with different functionalities ranging from printing vouchers from retailer side, syncing local and remote data, and managing users from the distributor side.",
    tools: [
      tools.flutter,
      tools.dart,
      tools.sqlite,
      tools.hasura,
      tools.vuejs,
      tools.graphql,
      tools.gitlab,
    ],
  },
  {
    slug: "telebirr-agent",
    name: "Telebirr Agent Manager",
    duration: "Dec 2020 - Present",
    description:
      "A Project that uses location and other shop information to register and manage registered telebirr shop agents.",
    tools: [
      tools.flutter,
      tools.dart,
      tools.hasura,
      tools.vuejs,
      tools.graphql,
      tools.gitlab,
    ],
  },
];

export let thabet_projects: IProject[] = [
  {
    slug: "gobez",
    name: "Gobez",
    duration: "Jun 2021 - Oct 2022",
    description:
      "Gobez is a platform that mainly provides technologically advanced system in regards to employment in the informal sector. The platform helps employers to hire domestic workers without wasting their time and energy. It is mainly aimed to solve one of the most critical issues regarding high unemployment rate in the informal sector and to prevent the crowd and unprofessionalism available while hiring domestic workers.",
    tools: [
      tools.flutter,
      tools.dart,
      tools.sqlite,
      tools.firebase,
      tools.appstore,
      tools.playstore,
      tools.gitlab,
    ],
  },
  {
    slug: "cdi",
    name: "CDI24 Portal",
    duration: "Jul 2021 - Sep 2021",
    description:
      "CDI24 is an online learning platform , where ethiopian students across the world take training on courses like mulesoft, sharepoint , java and the like.",
    tools: [
      tools.flutter,
      tools.dart,
      tools.sqlite,
      tools.firebase,
      tools.appstore,
      tools.playstore,
      tools.gitlab,
    ],
  },
];

export let all_projects: IProject[] = [
  ...thabet_projects,
  ...code_light_projects,
  ...gcme_projects,
  ...minab_projects,
];

// export let other_projects:IProject = [];
