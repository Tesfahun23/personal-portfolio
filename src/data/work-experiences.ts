import { IWorkExperience } from "../../types/work";
import {
  minab_projects,
  code_light_projects,
  gcme_projects,
  thabet_projects,
} from "./projects";

let jobs: IWorkExperience[] = [
  {
    company: "Minab IT Solutions",
    position: "Junior Software Developer",
    duration: "Dec 2019 - Feb 2021",
    responsiblities: [
      "Developed and maintained code for client mobile and web apps using  flutter and vuejs.",
      "Translated design mockups or wireframes into functional code.",
      "Worked with a team of four developers to build screening platform for  Jobs Creation Commission , and digital content sharing platform for universities & TVET students.",
      "Integrate Google Analytics to a website.",
    ],
    projects: minab_projects,
  },
  {
    company: "Great Commission Ministry Ethiopia",
    position: "Flutter Developer",
    duration: "Aug 2020 - Dec 2020",
    responsiblities: [
      "Translated design mockups or wireframes into functional code.",
      "Implemented data synchronization with API and vice versa. ",
      "Implemented Social Login, Video Streaming and Downloading.",
    ],
    projects: gcme_projects,
  },
  {
    company: "Codelight Software Engineering",
    position: "Front End Developer",
    duration: "Dec 2020 - Present",
    responsiblities: [
      "Designed and Implement a responsive UI/UX using  flutter, dart & vuejs.",
      "Wrote flutter platform channels for Bluetooth Low Energy Printer Devices.",
      "Wrote an SDK for flutter to work with hybrid POS machines.",
      "Implemented encrypted database for local data storage.",
    ],
    projects: code_light_projects,
  },
  {
    company: "Thabet Technologies",
    position: "Senior Mobile Developer",
    duration: "Jun 2021 - Oct 2022",
    responsiblities: [
      "Designed & Implemented UI/UX using flutter.",
      "Integrated Firebase services like Analytics , PhoneAuth, FCM.",
      "Integrated InApp purchase for apple store.",
    ],
    projects: thabet_projects,
  },
];

jobs.reverse();

export default jobs;
