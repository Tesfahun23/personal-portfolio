import { FaJava } from "react-icons/fa";
import { ITool } from "../../types/tools";
import {
  SiKotlin,
  SiJavascript,
  SiGithub,
  SiGitlab,
  SiVuetify,
  SiNuxtdotjs,
  SiHasura,
  SiGraphql,
  SiPostgresql,
  SiSqlite,
  SiMongodb,
  SiDocker,
  SiNetlify,
  SiVercel,
  SiCodemagic,
  SiFirebase,
  SiNodedotjs,
  SiAppstore,
  SiDart,
  SiGoogleplay,
  SiLoopback,
  SiFlutter,
  SiVuedotjs,
  SiExpress,
  SiBitbucket
  
} from "react-icons/si";

let iconSize: number = 18;
interface IToolWrapper {
  flutter: ITool;
  dart: ITool;
  java: ITool;
  kotlin: ITool;
  javascript: ITool;
  github: ITool;
  gitlab: ITool;
  vuejs: ITool;
  nuxtjs: ITool;
  graphql: ITool;
  postgresql: ITool;
  sqlite: ITool;
  mongodb: ITool;
  docker: ITool;
  netlify: ITool;
  vercel: ITool;
  codemagic: ITool;
  firebase: ITool;
  nodejs: ITool;
  express: ITool;
  hasura: ITool;
  playstore: ITool;
  appstore: ITool;
  loopback: ITool;
  vuetify: ITool;
  bitbucket: ITool;
}

let tools: IToolWrapper  = {
  flutter: {
    name: "Flutter",
    icon: <SiFlutter size={iconSize} />,
  },
  dart: {
    name: "Dart",
    icon: <SiDart size={iconSize} />,
  },
  java: {
    name: "Java",
    icon: <FaJava size={iconSize} />,
  },
  kotlin: {
    name: "Kotlin",
    icon: <SiKotlin size={iconSize} />,
  },
  javascript: {
    name: "Javascript",
    icon: <SiJavascript size={iconSize} />,
  },
  github: {
    name: "Github",
    icon: <SiGithub size={iconSize} />,
  },
  gitlab: {
    name: "Gitlab",
    icon: <SiGitlab size={iconSize} />,
  },
  vuejs: {
    name: "VueJs",
    icon: <SiVuedotjs size={iconSize} />,
  },
  nuxtjs: {
    name: "NuxtJS",
    icon: <SiNuxtdotjs size={iconSize} />,
  },
  graphql: {
    name: "GraphQL",
    icon: <SiGraphql size={iconSize} />,
  },
  postgresql: {
    name: "Postgresql",
    icon: <SiPostgresql size={iconSize} />,
  },
  sqlite: {
    name: "Sqlite",
    icon: <SiSqlite size={iconSize} />,
  },
  mongodb: {
    name: "Mongodb",
    icon: <SiMongodb size={iconSize} />,
  },
  docker: {
    name: "Docker",
    icon: <SiDocker size={iconSize} />,
  },
  netlify: {
    name: "Netlify",
    icon: <SiNetlify size={iconSize} />,
  },
  vercel: {
    name: "Vercel",
    icon: <SiVercel size={iconSize} />,
  },
  codemagic: {
    name: "Codemagic",
    icon: <SiCodemagic size={iconSize} />,
  },
  firebase: {
    name: "Firebase",
    icon: <SiFirebase size={iconSize} />,
  },
  nodejs: {
    name: "NodeJS",
    icon: <SiNodedotjs size={iconSize} />,
  },
  hasura: {
    name: "Hasura",
    icon: <SiHasura size={iconSize} />,
  },
  playstore: {
    name: "Google Play Console",
    icon: <SiGoogleplay size={iconSize} />,
  },
  appstore: {
    name: "Apple Store Console",
    icon: <SiAppstore size={iconSize} />,
  },
  loopback: {
    name: "Loopback",
    icon: <SiLoopback size={iconSize} />,
  },
  vuetify: {
    name: "Vuetify",
    icon: <SiVuetify size={iconSize} />,
  },
  express: {
    name: "ExpressJs",
    icon: <SiExpress size={iconSize} />,
  },
  bitbucket: {
    name: "Bitbucket",
    icon: <SiBitbucket size={iconSize} />,
  },
};



export default tools;
