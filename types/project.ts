import { ITool } from "./tools";

export interface IProject {
  slug: string;
  name: string;
  duration: string;
  description: string;
  tools: ITool[];
}
