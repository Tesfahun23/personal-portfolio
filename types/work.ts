import { IProject } from "./project";

export interface IWorkExperience {
  company: string;
  position: string;
  duration: string;
  responsiblities: string[];
  projects: IProject[];
}
