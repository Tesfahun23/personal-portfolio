import React from "react";

export interface ITool {
  icon: React.ReactNode;
  name: string;
}
